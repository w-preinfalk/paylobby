package at.paxfu.ls.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class scoreboardgetter {

    public static String getLine1() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "1");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine2() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "2");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine3() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "3");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine4() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "4");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine5() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "5");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine6() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "6");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine7() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "7");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine8() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "8");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine9() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "9");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine10() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "10");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine11() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "11");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine12() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "12");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine13() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "13");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String getLine14() {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_scoreboard` WHERE `systemid` = ?", new HashMap<Integer, String>(){
            {
                put(1, "14");
            }
        });
        try {
            prefix.next();
            return prefix.getString("value");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }


}
