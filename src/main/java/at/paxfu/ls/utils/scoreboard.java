package at.paxfu.ls.utils;

import at.paxfu.ls.LobbySystem;
import fr.minuskube.netherboard.Netherboard;
import fr.minuskube.netherboard.bukkit.BPlayerBoard;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class scoreboard {

    public static void setScoreboard(Player player) {
        BPlayerBoard board = Netherboard.instance().createBoard(player, mysqlgetter.getScoreboardName());

        board.setAll(
                scoreboardgetter.getLine1(),
                scoreboardgetter.getLine2(),
                scoreboardgetter.getLine3(),
                scoreboardgetter.getLine4(),
                scoreboardgetter.getLine5(),
                scoreboardgetter.getLine6(),
                scoreboardgetter.getLine7(),
                scoreboardgetter.getLine8(),
                scoreboardgetter.getLine9(),
                scoreboardgetter.getLine10(),
                scoreboardgetter.getLine11(),
                scoreboardgetter.getLine12(),
                scoreboardgetter.getLine13(),
                scoreboardgetter.getLine14()
        );
    }

    public static void scoreboardScheduler() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(LobbySystem.getInstance(), new Runnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    BPlayerBoard board = Netherboard.instance().createBoard(player, mysqlgetter.getScoreboardName());

                    board.setAll(
                            scoreboardgetter.getLine1(),
                            scoreboardgetter.getLine2(),
                            scoreboardgetter.getLine3(),
                            scoreboardgetter.getLine4(),
                            scoreboardgetter.getLine5(),
                            scoreboardgetter.getLine6(),
                            scoreboardgetter.getLine7(),
                            scoreboardgetter.getLine8(),
                            scoreboardgetter.getLine9(),
                            scoreboardgetter.getLine10(),
                            scoreboardgetter.getLine11(),
                            scoreboardgetter.getLine12(),
                            scoreboardgetter.getLine13(),
                            scoreboardgetter.getLine14()
                    );
                }
            }
        }, 0L, 200L);
    }
}
