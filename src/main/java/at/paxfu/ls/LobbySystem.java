package at.paxfu.ls;

import at.paxfu.ls.commands.createCMD;
import at.paxfu.ls.commands.setspawnCMD;
import at.paxfu.ls.listeners.*;
import at.paxfu.ls.utils.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import at.paxfu.ls.utils.scoreboard;
import at.paxfu.ls.utils.mysqlgetter;

import java.io.File;
import java.io.IOException;

public class LobbySystem extends JavaPlugin {

    public static File locationsf, mysqlf;
    public static FileConfiguration locations, mysql;
    public static String mysqlcon = "";
    public static String spawnset = "";



    @Override
    public void onEnable() {

        instance = this;


        try {
            createFiles();
            registerEvents();
            registerCommands();

            if(LobbySystem.mysql.getString("host") != null) {
                mysqlcon = "§aCONNECTED";
            }else {
                mysqlcon = "§cERROR";
            }
            if(LobbySystem.locations.getString("Spawn.X") != null) {
                spawnset = "§aSET";
            }else {
                spawnset = "§cNOT SET";
            }

            Bukkit.getConsoleSender().sendMessage("§8-----------------------------------");
            Bukkit.getConsoleSender().sendMessage("§6VELO-CODE LOBBY-SYSTEM v.1.0 §8BETA");
            Bukkit.getConsoleSender().sendMessage("");
            Bukkit.getConsoleSender().sendMessage("§6MySQL: " + mysqlcon);
            Bukkit.getConsoleSender().sendMessage("");
            Bukkit.getConsoleSender().sendMessage("§6Spawnpoint: " + spawnset);
            Bukkit.getConsoleSender().sendMessage("");
            Bukkit.getConsoleSender().sendMessage("§8Made by VELO-CODE");
            Bukkit.getConsoleSender().sendMessage("§8All rights reserved");
            Bukkit.getConsoleSender().sendMessage("§8Employees: paxfu, wave");
            Bukkit.getConsoleSender().sendMessage("§8-----------------------------------");
        }catch (Exception e) {
            Bukkit.getConsoleSender().sendMessage(mysqlgetter.getPREFIX() + "§4ERROR §7- §7Please contact a developer");
        }

            if(mysqlgetter.ifScoreboardEnabled().equals(1)) {
                scoreboard.scoreboardScheduler();
            }
    }

    private void registerCommands() {
        getCommand("create").setExecutor(new createCMD());
        getCommand("setspawn").setExecutor(new setspawnCMD());
    }

    @Override
    public void onDisable() {

    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new joinListener(), this);
        Bukkit.getPluginManager().registerEvents(new quitListener(), this);
        Bukkit.getPluginManager().registerEvents(new breakBlockListener(), this);
        Bukkit.getPluginManager().registerEvents(new foodListener(), this);
        Bukkit.getPluginManager().registerEvents(new onDrop(), this);
        Bukkit.getPluginManager().registerEvents(new onPlace(), this);
        Bukkit.getPluginManager().registerEvents(new onDamage(), this);
        Bukkit.getPluginManager().registerEvents(new invMove(), this);
    }

    //Files
    private void createFiles(){
        locationsf = new File(getDataFolder(), "locations.yml");
        mysqlf = new File(getDataFolder(), "mysql.yml");


        if (!locationsf.exists()){
            locationsf.getParentFile().mkdirs();
            saveResource("locations.yml", false);
        }


        if (!mysqlf.exists()){
            mysqlf.getParentFile().mkdirs();
            saveResource("mysql.yml", false);
        }

        locations = new YamlConfiguration();
        mysql = new YamlConfiguration();

        try {
            locations.load(locationsf);
            mysql.load(mysqlf);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }



    public static LobbySystem instance;

    public static LobbySystem getInstance() {
        return instance;
    }
}
