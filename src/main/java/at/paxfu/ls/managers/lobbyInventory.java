package at.paxfu.ls.managers;

import at.paxfu.ls.LobbySystem;
import at.paxfu.ls.api.hotbarAPI;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class lobbyInventory {

    public static void setInventory(Player player) {

        int i0 = hotbarAPI.getHotbarItemID(0);
        ItemStack item0 = new ItemStack(Material.getMaterial(i0), 1, (short) 7);
        ItemMeta item0meta = item0.getItemMeta();
        item0meta.setDisplayName(hotbarAPI.getHotbarItemName(0));
        item0.setItemMeta(item0meta);

        int i1 = hotbarAPI.getHotbarItemID(1);
        ItemStack item1 = new ItemStack(Material.getMaterial(i1));
        ItemMeta item1meta = item1.getItemMeta();
        item1meta.setDisplayName(hotbarAPI.getHotbarItemName(1));
        item1.setItemMeta(item1meta);

        int i2 = hotbarAPI.getHotbarItemID(2);
        ItemStack item2 = new ItemStack(Material.getMaterial(i2), 1, (short) 7);
        ItemMeta item2meta = item2.getItemMeta();
        item2meta.setDisplayName(hotbarAPI.getHotbarItemName(2));
        item2.setItemMeta(item2meta);

        int i3 = hotbarAPI.getHotbarItemID(3);
        ItemStack item3 = new ItemStack(Material.getMaterial(i3));
        ItemMeta item3meta = item3.getItemMeta();
        item3meta.setDisplayName(hotbarAPI.getHotbarItemName(3));
        item3.setItemMeta(item3meta);

        int i4 = hotbarAPI.getHotbarItemID(4);
        ItemStack item4 = new ItemStack(Material.getMaterial(i4));
        ItemMeta item4meta = item4.getItemMeta();
        item4meta.setDisplayName(hotbarAPI.getHotbarItemName(4));
        item4.setItemMeta(item4meta);

        int i5 = hotbarAPI.getHotbarItemID(5);
        ItemStack item5 = new ItemStack(Material.getMaterial(i5), 1, (short) 7);
        ItemMeta item5meta = item5.getItemMeta();
        item5meta.setDisplayName(hotbarAPI.getHotbarItemName(5));
        item5.setItemMeta(item5meta);

        int i6 = hotbarAPI.getHotbarItemID(6);
        ItemStack item6 = new ItemStack(Material.getMaterial(i6), 1, (short) 7);
        ItemMeta item6meta = item6.getItemMeta();
        item6meta.setDisplayName(hotbarAPI.getHotbarItemName(6));
        item6.setItemMeta(item6meta);

        int i7 = hotbarAPI.getHotbarItemID(7);
        ItemStack item7 = new ItemStack(Material.getMaterial(i7));
        ItemMeta item7meta = item7.getItemMeta();
        item7meta.setDisplayName(hotbarAPI.getHotbarItemName(7));
        item7.setItemMeta(item7meta);

        int i8 = hotbarAPI.getHotbarItemID(8);
        ItemStack item8 = new ItemStack(Material.getMaterial(i8));
        ItemMeta item8meta = item8.getItemMeta();
        item8meta.setDisplayName(hotbarAPI.getHotbarItemName(8));
        item8.setItemMeta(item8meta);


        player.getInventory().setItem(0, item0);
        player.getInventory().setItem(1, item1);
        player.getInventory().setItem(2, item2);
        player.getInventory().setItem(3, item3);
        player.getInventory().setItem(4, item4);
        player.getInventory().setItem(5, item5);
        player.getInventory().setItem(6, item6);
        player.getInventory().setItem(7, item7);
        player.getInventory().setItem(8, item8);
    }
}
