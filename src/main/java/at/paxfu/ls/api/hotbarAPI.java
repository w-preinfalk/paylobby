package at.paxfu.ls.api;

import at.paxfu.ls.utils.MySQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class hotbarAPI {

    public static int getHotbarItemID(Integer slot) {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_hotbar` WHERE `slot` = ?", new HashMap<Integer, String>(){
            {
                put(1, String.valueOf(slot));
            }
        });
        try {
            prefix.next();
            return prefix.getInt("itemid");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return 1;
        }
    }

    public static String getHotbarItemName(Integer slot) {
        ResultSet prefix = MySQL.getInstance().executeQuery("SELECT * FROM `lobby_hotbar` WHERE `slot` = ?", new HashMap<Integer, String>(){
            {
                put(1, String.valueOf(slot));
            }
        });
        try {
            prefix.next();
            return prefix.getString("displayname");
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }
}
